package com.book.search.client;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import com.book.domain.*;
import com.book.search.service.searchservice;

public class SearchClient {

			
	 public static void main(String args[]) throws Exception {

	    	JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();

	    	factory.getInInterceptors().add(new LoggingInInterceptor());
	    	factory.getOutInterceptors().add(new LoggingOutInterceptor());
	    	factory.setBindingId("http://cxf.apache.org/bindings/xformat");
	    	factory.setServiceClass(searchservice.class);
	    	factory.setAddress("http://localhost:8080/Comp_433_project3/services/search");
	    	searchservice client = (searchservice) factory.create();
	    	
	    	Book book = client.getBook(1111);
	    	System.out.println("The Book name is: " + book.getBookName());
	    	System.exit(0);

	    }
}
