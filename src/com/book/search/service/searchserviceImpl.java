package com.book.search.service;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import com.book.domain.*;


@SuppressWarnings("unused")
@WebService(endpointInterface = "com.book.search.service.searchservice", serviceName = "searchService")
public class searchserviceImpl implements searchservice {

	public Book getBook(Integer isbn) {
		BookDAO dao = new BookDAO();
		return dao.getBook(isbn);
	}

	
	

}

