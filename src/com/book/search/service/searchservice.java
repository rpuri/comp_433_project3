package com.book.search.service;

import javax.jws.WebService;
import javax.jws.WebParam;

import com.book.domain.*;


	@WebService
	public interface searchservice {
	    Book getBook(@WebParam(name="isbn") Integer isbn);
	}

