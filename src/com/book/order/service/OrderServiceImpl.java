package com.book.order.service;

import java.util.ArrayList;

import javax.jws.WebParam;
import javax.jws.WebService;

import com.book.place.order.*;
import com.book.domain.*;

@SuppressWarnings("unused")
@WebService(endpointInterface = "com.book.order.service.Orderservice", serviceName = "orderService")
public class OrderServiceImpl implements Orderservice {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	/*public String setOrder(ArrayList orderBooks,Address shipAddr,Address billAddr,double totalinprice){
		OrderDAO OrderDAO1 = new OrderDAO();
		return OrderDAO1.setOrder(orderBooks, shipAddr, billAddr, totalinprice);*/
	public Order setOrder(Order sentorder){
		OrderDAO OrderDAO1 = new OrderDAO();
		return OrderDAO1.setOrder(sentorder);
	
	}
	public String getOrderStatus(int orderNum){
		OrderDAO OrderDAO2 = new OrderDAO();
		return OrderDAO2.getOrderStatus(orderNum);
	}
	public String cancelOrder(int orderNum){
		OrderDAO OrderDAO2 = new OrderDAO();
		return OrderDAO2.cancelOrder(orderNum);
	}

	
}



