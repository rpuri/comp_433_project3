package com.book.order.service;


import java.util.ArrayList;

import javax.jws.WebService;
import javax.jws.WebParam;

import com.book.place.order.*;


	@WebService
	public interface Orderservice {
	    @SuppressWarnings("rawtypes")
	//	String setOrder(@WebParam(name="arraybooks") ArrayList orderBooks,@WebParam(name="shippingAddress") Address shipAddr,@WebParam(name="billingAddress") Address billAddr,@WebParam(name="totalprice") double totalinprice);
	    Order setOrder(@WebParam(name="Order") Order setorder);
	    String getOrderStatus(@WebParam(name="OrderNum1") int orderNum);
	    String cancelOrder(@WebParam(name="OrderNum2") int orderNum);
	}
//	public String setOrder(ArrayList<Book> orderBooks,Address shipAddr, Address billAddr,double totalinprice)

