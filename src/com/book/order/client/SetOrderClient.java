package com.book.order.client;


import java.util.ArrayList;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import com.book.domain.*;
import com.book.order.service.*;
import com.book.place.order.Address;
import com.book.place.order.Order;

public class SetOrderClient {

			
	 public static void main(String args[]) throws Exception {

	    	JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();

	    	factory.getInInterceptors().add(new LoggingInInterceptor());
	    	factory.getOutInterceptors().add(new LoggingOutInterceptor());
	    	factory.setBindingId("http://cxf.apache.org/bindings/xformat");
	    	factory.setServiceClass(Orderservice.class);
	    	factory.setAddress("http://localhost:8080/Comp_433_project3/services/order");
	    	Orderservice client = (Orderservice) factory.create();
	    			
	    	
	    	Order order1 = new Order();
			Address address1 = new Address();
			Address address2 = new Address();
			Book book1 = new Book();
			Book book2 = new Book();
			BookDAO bookDAO = new BookDAO();
			book1 = bookDAO.getBook(1115);
			book2 = bookDAO.getBook(1113);
			order1.addBook(book1);
			order1.addBook(book2);
			address1.setAddress1("802 N Michigan");
			address1.setAddress2("Pearson");
			address1.setCity("Chicago");
			address1.setState("IL");
			address1.setZip(60608);
			address2.setAddress1("ABCD Rd");
			address2.setAddress2("Michigan");
			address2.setCity("Chicago");
			address2.setState("IL");
			address2.setZip(60610);
			order1.setShipAddress(address1);
			order1.setBillAddress(address2);
			order1.setTotalprice(300);
			order1.setMessage(" ");
			int orderNumMax = 1;
			int orderNum1 = orderNumMax;
			order1.setOrderNum(orderNum1);
	    	System.out.println("The set Order Status is: " + client.setOrder(order1).getOrderStatus());
	    	
	    
	    	System.exit(0);

	    }
}
