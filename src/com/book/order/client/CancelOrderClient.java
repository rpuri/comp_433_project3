package com.book.order.client;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import com.book.order.service.Orderservice;

public class CancelOrderClient {
	 public static void main(String args[]) throws Exception {

	    	JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();

	    	factory.getInInterceptors().add(new LoggingInInterceptor());
	    	factory.getOutInterceptors().add(new LoggingOutInterceptor());
	    	factory.setBindingId("http://cxf.apache.org/bindings/xformat");
	    	factory.setServiceClass(Orderservice.class);
	    	factory.setAddress("http://localhost:8080/Comp_433_project3/services/order");
	    	Orderservice client = (Orderservice) factory.create();
	    	System.out.println("Cancel order result is" + client.cancelOrder(2));		
	 }	
}
